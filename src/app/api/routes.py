from aiohttp import web
import aiohttp_cors
from app.api.handlers.handlers import welcome_handle, version
from app.api.handlers.vk_handler import vk_handler
from typing import NoReturn


def setup_routes(app: web.Application) -> NoReturn:
    """Setup endpoints for application"""
    cors = aiohttp_cors.setup(app, defaults={
        "*": aiohttp_cors.ResourceOptions(
            allow_credentials=True,
            expose_headers="*",
            allow_headers="*"
        )
    })

    app.router.add_route("GET", "/", welcome_handle)
    app.router.add_route("GET", "/{name}", welcome_handle)
    app.router.add_route("GET", "/api/version", version)
    app.router.add_route("GET", "/{group_link}/{count}", vk_handler)

    for route in list(app.router.routes()):
        cors.add(route)
