import os
from pathlib import Path
from typing import Optional, NoReturn
import tensorflow as tf
import pandas as pd
import numpy as np
import pickle
from gensim.models import KeyedVectors
from app.api.handlers.data_handle import HandleData


class TfSentimentModel:
    mapper = {-1: "negative",
              0: "neutral",
              1: "positive"
              }

    def __init__(self, path_to_data: str = "data"):
        """
        This is init

        :param path_to_data: path to data, ls:
        data
            models
                dl_model
                fasttext
            lemmas.pkl
        """
        self.__PATH_TO_DATA = path_to_data
        self.fasttext_model: Optional[KeyedVectors] = None
        self.tf_model: Optional[tf.keras.Model] = None
        self.data_handler: Optional[HandleData] = None

        self.set_fasttext_model(
            os.path.join(path_to_data, "models", "fasttext", "model.model")
        )

        self.set_data_handler(
            os.path.join(path_to_data, "lemmas.pkl")
        )

        self.set_tf_model(
            os.path.join(path_to_data, "models", "dl_model")
        )

    def set_fasttext_model(self, fasttext_path: str) -> NoReturn:
        """
        Load fasttext model from folder

        :param fasttext_path: path to fast_text_model
        :return: None
        """
        self.fasttext_model: KeyedVectors = KeyedVectors.load(fasttext_path)

    def set_tf_model(self, tf_model_path: str) -> NoReturn:
        """
        Load TF model from folder

        :param tf_model_path: path to saved tensorflow model
        :return: None
        """
        self.tf_model: tf.keras.Model = tf.keras.models.load_model(tf_model_path)

    def set_data_handler(self, lemmas_path: str = None) -> NoReturn:
        """
        Handle data

        :param lemmas_path: path to dictionary (from raw word to the lemma)
        :return: None
        """
        self.data_handler: HandleData = HandleData()
        if lemmas_path is not None:
            with open(lemmas_path, "rb") as lemmas_dict:
                self.data_handler.lemmas_dict = pickle.load(lemmas_dict)

    def predict_proba(self, text: str) -> np.ndarray:
        """Get probabilities predict"""
        prepared_text = self.data_handler.transform_table(
            pd.Series(text),
            self.fasttext_model
        )
        predict_proba = self.tf_model.predict(prepared_text)
        return predict_proba

    def predict(self, text: str) -> str:
        """Get class predict, based on probabilities"""
        predict_proba = self.predict_proba(text)[0]
        predict_class = np.argmax(predict_proba, axis=-1)
        predict_label = self.mapper[predict_class - 1]
        return predict_label


if __name__ == "__main__":
    PATH_TO_DATA = os.path.join(
        Path(__file__).parent.parent.parent.parent, "data"
        )

    model = TfSentimentModel(PATH_TO_DATA)

    while True:
        message = input("print tested message or exit: ")
        if message == "exit":
            break
        print(model.predict(message))
