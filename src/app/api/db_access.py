import asyncpg
from typing import NoReturn


async def create_table(pool: asyncpg.pool.Pool) -> NoReturn:
    """Create table in postgre"""
    async with pool.acquire() as connection:
        await connection.execute(
            '''
            CREATE TABLE IF NOT EXISTS posts (
            group_link varchar(100),
            post_id int, 
            text varchar(15895),
            sentiment varchar(8)
            )
            '''
            )
