import vk_api
import pathlib
from aiohttp import web
from app.api.dl_model import TfSentimentModel
from app.api.handlers.db_handler import insert_into_posts


def auth_handler() -> tuple:
    """При двухфакторной аутентификации вызывается эта функция."""
    # Код двухфакторной аутентификации
    key = input("Enter authentication code: ")

    remember_device = False  # Если: True - сохранить, False - не сохранять.

    return key, remember_device


def give_me_a_power(login: str, password: str) -> vk_api.vk_api.VkApiMethod:
    """Access to VK"""
    vk_session = vk_api.VkApi(
        login, password,
        # функция для обработки двухфакторной аутентификации
        auth_handler=auth_handler
    )

    try:
        vk_session.auth()
    except vk_api.AuthError as error_msg:
        print(error_msg)

    return vk_session.get_api()


def get_posts(vk: vk_api.vk_api.VkApiMethod,
              group_link: str, count: int
              ) -> dict:
    """Get *count* posts from *group_link* in vk"""
    nums, rem = count // 100 + 1, count % 100
    history = dict()

    value = 100
    for shift in range(nums):
        if shift == nums - 1:
            value = rem

        tmp = vk.wall.get(domain=group_link, offset=shift * 100, count=value)

        history.update(
            {line["id"]: line["text"] for line in tmp["items"] if line["text"] != ""}
        )

    return history


async def vk_handler(request: web.Request) -> web.Response:
    """VK posts sentiment handler"""
    model = request.app['tf_model']
    group_link = request.match_info.get("group_link")
    count = request.match_info.get("count")
    try:
        count = int(count)
        if count <= 0:
            raise ValueError
    except ValueError:
        text = "For sentiment analysis, count must be positive integer"
        return web.Response(text=text)

    vk: vk_api.vk_api.VkApiMethod = give_me_a_power(*request.app["vk_login_password"])
    posts: dict = get_posts(vk, group_link, count)
    history = [f"{group_link}:"]

    for post_id, text in posts.items():
        sentiment_predict = model.predict(text)
        history += [f"{post_id}:\n{text}\n{sentiment_predict}\n"]

        await insert_into_posts(
            request.app["pool"],
            (group_link, post_id, text, sentiment_predict)
        )

    text = "\n".join(history)
    return web.Response(text=text)


if __name__ == "__main__":
    api: vk_api.vk_api.VkApiMethod = give_me_a_power()
    ans = get_posts(api, "rnm31", 3)


    print(PATH_TO_DATA)
    tf_model = TfSentimentModel(PATH_TO_DATA)
    for post, message in ans.items():
        print(post)
        print(message)
        print(tf_model.predict(message))
        print()
