import asyncpg
from typing import NoReturn


async def insert_into_posts(pool: asyncpg.pool.Pool, values: tuple) -> NoReturn:
    """Insert raw in postgresql"""
    group_link, post_id, text, sentiment = values
    async with pool.acquire() as connection:
        await connection.execute(
            f"""
            INSERT INTO posts(group_link, post_id, text, sentiment) 
            VALUES('{group_link}', {post_id}, '{text}', '{sentiment}');
            """
        )


if __name__ == "__main__":
    print("Hello ")
