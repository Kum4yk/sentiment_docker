from aiohttp import web


async def welcome_handle(request: web.Request) -> web.Response:
    """Handle welcome page"""
    name = request.match_info.get('name', "Anonymous")
    body = """

It is a simple service for VK sentiment analysis.
    
Possible routes:
/{name} - welcome page by any name
/api/version - the service version
/{group_link}/{post_number} - post numbers from vk wall by group_link (or user link) to sentiment analysis
"""
    text = "Hello, " + name + body
    return web.Response(text=text)


async def version(request: web.Request) -> web.Response:
    """Возвращает номер версии сервиса"""
    config = request.app["config"]
    return web.Response(text=config["service"]["version"])
