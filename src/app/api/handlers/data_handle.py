from pymystem3 import Mystem
import pandas as pd
import xmltodict
import re
import numpy as np
from gensim.models import KeyedVectors


class HandleData:
    companies = {}
    lemmas_dict = dict()
    mystem = Mystem()
    COLUMN_NUMS = 31

    @staticmethod
    def get_sample_text(sample: pd.DataFrame) -> pd.Series:
        """Get text feature"""
        assert sample['column'][3]['@name'] == 'text'
        return sample['column'][3]['#text']

    @staticmethod
    def get_sample_answers_bank(sample: pd.DataFrame) -> dict:
        """Create answers dict for bank"""
        answers = {}
        for i in range(4, 12):
            HandleData.companies[sample['column'][i]['@name']] = i
            answers[sample['column'][i]['@name']] = None if sample['column'][i]['#text'] == 'NULL' \
                else int(sample['column'][i]['#text'])
        return answers

    @staticmethod
    def get_sample_answers_tkk(sample: pd.DataFrame) -> dict:
        """Create answers dict for tkk"""
        answers = {}
        for i in range(4, 11):
            HandleData.companies[sample['column'][i]['@name']] = i
            answers[sample['column'][i]['@name']] = None if sample['column'][i]['#text'] == 'NULL' \
                else int(sample['column'][i]['#text'])
        return answers

    @staticmethod
    def get_sample_id(sample: pd.DataFrame) -> int:
        """Get id feature"""
        assert sample['column'][0]['@name'] == 'id'
        return int(sample['column'][0]['#text'])

    @staticmethod
    def get_data(filename: str, answer_func=None) -> pd.DataFrame:
        """Get data"""
        df = pd.DataFrame()
        with open(filename, "r", encoding='utf-8') as f:
            d = xmltodict.parse(f.read(), process_namespaces=True)
            clean_samples = []
            for sample in d['pma_xml_export']['database']['table']:
                sample_id = HandleData.get_sample_id(sample)
                text = HandleData.get_sample_text(sample)
                answers = answer_func(sample)
                for company, answer in answers.items():
                    if answer is not None:
                        clean_samples.append((sample_id, text, company, answer))
            df['text'] = [sample[1] for sample in clean_samples]
            df['answer'] = [sample[3] for sample in clean_samples]
            df['company'] = [sample[2] for sample in clean_samples]
            df['sample_id'] = [sample[0] for sample in clean_samples]
        return df

    @staticmethod
    def table_from_xml(xml_path: str, mode: str = None) -> tuple:
        """Get pd.DataFrames data from .xml files"""
        assert mode in {"tkk", "bank"}

        func = HandleData.get_sample_answers_tkk \
            if mode == "tkk" else\
            HandleData.get_sample_answers_bank

        data = HandleData.get_data(xml_path, func)

        data['text'] = data['text'].apply(
            lambda x: re.sub(r'(?:http[^\s]+)($|\s)', r'url\1', x)
        )

        data['text'] = data['text'].apply(
            lambda x: re.sub(r'(?:@[^\s]+)($|\s)', r'user\1', x)
        )

        return data.text, data.answer

    @staticmethod
    def tag_by_dict(word: str, mystem: Mystem, words_dict: dict, debugging: bool = False) -> str:
        """Create dict with lemmas, for more faster function working"""
        if word is None or word == "":
            return ""
        if word in words_dict:
            return words_dict[word]
        if word.isdigit():
            words_dict[word] = word
            return word

        processed = mystem.analyze(word)[0]
        if "analysis" not in processed:
            if debugging:
                print(word, "- strange word, has not analysis")
            words_dict[word] = word
            return word

        lemma = processed["analysis"]
        if lemma:
            answer = lemma[0]["lex"].lower().strip()
            words_dict[word] = answer
            return answer

        if debugging:
            print(word, "- strange word, empty analysis")
        words_dict[word] = word
        return word

    @staticmethod
    def transform_table(table: pd.Series, fasttext_model: KeyedVectors,
                        debug_mode: bool = False
                        ) -> np.array:
        """Transform data table TF model input"""
        data: pd.DataFrame = table.str.lower().replace(r'[^\w\s]', value="", regex=True) \
            .str.split(expand=True)

        if len(data.columns) < HandleData.COLUMN_NUMS:
            for i in set(range(HandleData.COLUMN_NUMS)) - set(data.columns):
                data[i] = ""
        elif len(data.columns) > HandleData.COLUMN_NUMS:
            data = data.iloc[:, :HandleData.COLUMN_NUMS]

        data = data.applymap(
            lambda x: HandleData.tag_by_dict(x,
                                             HandleData.mystem,
                                             HandleData.lemmas_dict,
                                             debugging=debug_mode
                                             )
            )

        data = data.applymap(lambda word: np.array(fasttext_model[word]))

        data = np.array(
            [[np.array(line) for line in row] for row in data.values]
        )[:, :, :, np.newaxis]

        return data

    @staticmethod
    def get_tensor_y(y: np.array) -> np.array:
        """Get answer tensor from raw data"""
        mapper = {-1: np.array((1, 0, 0)),
                  0: np.array((0, 1, 0)),
                  1: np.array((0, 0, 1))
                  }
        tensor_y = np.array([mapper[i] for i in y])  # DataFrame.values not working correct

        return tensor_y


if __name__ == "__main__":
    print(type(HandleData.get_tensor_y))
