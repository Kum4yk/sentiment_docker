import logging
from typing import NoReturn


def init_logger(config: dict) -> NoReturn:
    """Logging initializer"""
    log_level = logging.DEBUG if config["service"]["debug"] else logging.INFO
    logging.basicConfig(
        level=log_level,
        format=(
            f"{config['service']['name']} - "
            f"{config['service']['version']} - "
            '%(asctime)s - %'
            '%(levelname)s -'
            '%(message)s'
        ),
    )
