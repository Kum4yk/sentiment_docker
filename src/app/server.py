import asyncio
import asyncpg
import os
from pathlib import Path
from typing import List
from aiohttp import web
from app.api.dl_model import TfSentimentModel
from app.api.routes import setup_routes
from app.setup_service.configuration import get_config
from app.setup_service.logs import init_logger
from app.setup_service.middlewares import log_middleware, error_middleware
from app.api.db_access import create_table


async def init_app(model: TfSentimentModel, sql_params: dict, args: List[str]) -> web.Application:
    """Initialize the application server."""
    app = web.Application(
        middlewares=[log_middleware, error_middleware]
    )
    app["config"] = get_config(args)
    app["vk_login_password"]: List[str] = args

    # create connection
    app["pool"]: asyncpg.pool.Pool = await asyncpg.create_pool(**sql_params)
    await create_table(app["pool"])

    app["tf_model"] = model
    setup_routes(app)
    init_logger(app["config"])
    return app


if __name__ == "__main__":
    if os.sep == "\\":
        params = {
            'database': "vk-api_hm",
            'host': 'localhost',
            'user': 'postgres',
            'password': '123123'
        }
    else:
        params = {
            'database': 'docker_hm',
            'host': 'docker_ip',
            'user': 'postgres',
            'password': 'password'
        }
    print(params)

    PATH_TO_DATA = os.path.join(
        Path(__file__).parent.parent.parent, "data"
        )

    final_model = TfSentimentModel(PATH_TO_DATA)

    meta_args = []
    loop = asyncio.get_event_loop()
    application = loop.run_until_complete(
        init_app(
            final_model,
            params,
            meta_args
        )
    )

    print("\n\n\nGo here - http://localhost:8081/")

    web.run_app(
        application,
        port=application["config"]["service"]["port"],
        access_log=None
    )
