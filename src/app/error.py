from http import HTTPStatus


class ServiceError(Exception):
    """Base class"""

    def __init__(
            self,
            message: str = "Unexpected error",
            http_code: HTTPStatus = HTTPStatus.INTERNAL_SERVER_ERROR
    ):
        """Class for handmade service errors"""
        super().__init__(message)
        self.http_code = http_code
        self.message = message
