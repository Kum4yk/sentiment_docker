FROM python:3.8

RUN mkdir -p /sentiment_docker
WORKDIR /sentiment_docker
COPY . /sentiment_docker

RUN curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python
RUN ln -s /root/.poetry/bin/poetry /usr/bin/poetry
ENV PYTHONPATH "${PYTHONPATH}:/sentiment_docker/src"
RUN poetry config virtualenvs.create false
RUN poetry install

EXPOSE 8081

CMD ["python", "/sentiment_docker/src/app/server.py"]
